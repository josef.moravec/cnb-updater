SET NAMES utf8;
SET foreign_key_checks = 0;
SET time_zone = 'SYSTEM';
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `data`;
CREATE TABLE `data` (
    `id`            int(11)        NOT NULL AUTO_INCREMENT,
    `sigla`         varchar(6)     NOT NULL,
    `local_id`      varchar(64)    NOT NULL,
    `subfield`      enum ('a','z') NOT NULL,
    `value`         varchar(64)    NOT NULL,
    `date_modified` timestamp      DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `sigla_local_id` (`sigla`, `local_id`),
    KEY `date_modified` (`date_modified`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

DROP TABLE IF EXISTS `week_data`;
CREATE TABLE `week_data` (
    `id`            int(11)        NOT NULL AUTO_INCREMENT,
    `sigla`         varchar(6)     NOT NULL,
    `local_id`      varchar(64)    NOT NULL,
    `subfield`      enum ('a','z') NOT NULL,
    `week`          int(11)        NOT NULL,
    `value`         varchar(64)    NOT NULL,
    `date_modified` timestamp      DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    UNIQUE KEY `sigla_local_id_week` (`sigla`, `local_id`, `week`),
    KEY `week_idx` (`week`),
    KEY `date_modified` (`date_modified`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4;

SET foreign_key_checks = 1;



