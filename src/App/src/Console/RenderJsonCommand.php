<?php

declare(strict_types=1);
/**
 * Class RenderJsonCommand
 *
 * PHP version 7
 *
 * Copyright (C) Moravian Library 2020.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  App\Console
 * @author   Josef Moravec <moravec@mzk.cz>
 * @license  https://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://knihovny.cz Main Page
 */

namespace App\Console;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\Sql\Sql;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RenderJsonCommand  extends Command
{
    use LockableTrait;

    /**
     * @var string Command name
     */
    protected static $defaultName = 'render:json';

    /**
     * @var Adapter Database connection
     */
    protected Adapter $db;

    /**
     * RenderJsonCommand constructor.
     *
     * @param string|null $name Command name
     * @param Adapter     $db   Database connection
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    public function __construct(string $name = null, Adapter $db)
    {
        $this->db = $db;
        parent::__construct($name);
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setDescription('Render JSON for library')
            ->setHelp(
                'This command renders json with CNB data for a library (identified by SIGLA)'
            )->addOption('sigla', 's', InputOption::VALUE_REQUIRED, 'SIGLA identifier of library');
    }

    /**
     * Executes command
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return Command::SUCCESS;
        }

        $sigla = $input->getOption('sigla') ?? false;
        if ($sigla === false || $sigla === null) {
            $output->writeln('');
            $output->writeln('Sigla parameter is required');
            $output->writeln('');
            $output->writeln($this->getSynopsis());
            $output->writeln('');
            return Command::FAILURE;
        }

        $sql = new Sql($this->db, 'data');
        $select = $sql->select();
        $select->where(['sigla' => $sigla]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $json = [];
        if ($result instanceof ResultInterface && $result->isQueryResult()) {
            foreach ($result as $row) {
                $json[] = [
                    'id' => $row['local_id'],
                    'cnb' => $row['value'],
                ];
            }
        }
        echo json_encode($json, JSON_PRETTY_PRINT);

        $this->release();
        return Command::SUCCESS;
    }

}