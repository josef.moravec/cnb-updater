<?php

declare(strict_types=1);

/**
 * Class UpdateDatabaseCommand
 *
 * PHP version 7
 *
 * Copyright (C) Moravian Library 2020.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  App\Console
 * @author   Josef Moravec <moravec@mzk.cz>
 * @license  https://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://knihovny.cz Main Page
 */

namespace App\Console;

use App\Db\Updater;
use App\Ftp\Downloader;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateDatabaseCommand extends Command
{

    use LockableTrait;

    const CHUNK_SIZE = 1000;

    /**
     * @var string
     */
    protected static $defaultName = 'database:update';

    /**
     * @var Updater Database updater
     */
    protected Updater $dbUpdater;

    /**
     * @var Downloader Ftp downloader
     */
    protected Downloader $downloader;

    /**
     * UpdateDatabaseCommand constructor.
     *
     * @param string|null $name
     * @param Updater     $dbUpdater
     * @param Downloader  $downloader
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */

    public function __construct(string $name = null, Updater $dbUpdater, Downloader $downloader)
    {
        $this->dbUpdater = $dbUpdater;
        $this->downloader = $downloader;
        parent::__construct($name);
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setDescription('Update database of CNBs')
            ->setHelp(
                'This command updates the underlined database of CNBs for local record of libraries participated ' .
                'in union catalogue Caslin'
            )->addOption('week', 'w', InputOption::VALUE_REQUIRED, 'Update based on week data')
            ->addOption('full', 'f', InputOption::VALUE_NONE, 'If present the full (initial) update will be done');
    }

    /**
     * Executes command
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\InvalidArgumentException
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return Command::SUCCESS;
        }
        $full = $input->getOption('full');
        $week = $input->getOption('week') ?? false;
        if ($full) {
            $output->writeln('Running full update.');
            $filename = 'skc_cnb_m.csv.gz';
        } elseif ($week !== false && $week !== null ) {
            $output->writeln('Running weekly update for week ' . $week);
            $filename = sprintf('cnb_m_%02d.csv', intval($week));
        } else {
            $output->writeln('');
            $output->writeln('At least one option should be used');
            $output->writeln('');
            $output->writeln($this->getSynopsis());
            $output->writeln('');
            return Command::FAILURE;
        }

        try {
            $csv = $this->downloader->download($filename);
            $heading = fgetcsv($csv, 2048, ";"); // Read, but don't use heading

            if ($full) {
                $output->write('Deleting all data...');
                $this->dbUpdater->delete_all();
                $output->writeln(' done.');
            } else {
                $output->write('Deleting week data...');
                $this->dbUpdater->delete_week(intval($week));
                $output->writeln(' done.');
            }
            $data = [];
            $count = 1;
            $progressBar = new ProgressBar($output);
            $output->writeln('Inserting data');
            while (!feof($csv)) {
                $line = fgetcsv($csv, 2048, ";");
                if ($line === false) {
                    break;
                }
                $data = array_merge($data, $this->parse_row($line));
                if ($count++ % self::CHUNK_SIZE === 0) {
                    $this->commit($data, $full, intval($week));
                    $progressBar->advance(count($data));
                    $count = 1;
                    $data = [];
                }
            }
            // commit last data
            $this->commit($data, $full, intval($week));
            $progressBar->advance(count($data));
            $progressBar->finish();

        } catch (\Exception $e) {
            $output->writeln('There was an error during execution:');
            $output->writeln($e->getMessage());
            $output->writeln('');
        }

        $this->release();
        $output->writeln('');
        return Command::SUCCESS;
    }

    /**
     * Parse row from CSV
     *
     * @param array $line Cells from CSV
     *
     * @return array Structured array
     */

    protected function parse_row(array $line): array
    {
        $subfields = explode("$$", $line[2]);
        $subfields = array_filter($subfields, function ($s) {
            return ($s !== '') && (substr($s, 0, 1) === 'a');
        });
        return array_map(function ($subfield) use ($line) {
            return [
                'sigla' => $line[0],
                'localId' => $line[1],
                'subfield' => substr($subfield, 0, 1),
                'value' => substr($subfield, 1),
            ];
        }, $subfields);
    }

    /**
     * Commit data to the database
     *
     * @param array $data Data to commit
     * @param bool  $full Running in full update mode?
     * @param int   $week Week, if running in incremental update mode
     *
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     */
    protected function commit(array $data, bool $full = false, int $week = null): void
    {
        if ($full) {
            $this->dbUpdater->insert_data($data);
        } else {
            $this->dbUpdater->insert_week_data($data, $week);
        }
    }
}