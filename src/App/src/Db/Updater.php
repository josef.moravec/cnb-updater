<?php

declare(strict_types=1);

/**
 * Class Updater
 *
 * PHP version 7
 *
 * Copyright (C) Moravian Library 2020.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  App\Db
 * @author   Josef Moravec <moravec@mzk.cz>
 * @license  https://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://knihovny.cz Main Page
 */

namespace App\Db;

use Laminas\Db\Adapter\Adapter;

class Updater
{
    /** @var Adapter Database connection adapter */
    protected Adapter $db;

    /**
     * Updater constructor.
     *
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->db = $adapter;
    }

    /**
     * Update data
     *
     * @param array $data Array of associative arrays with keys: sigla, localId, subfield, value
     *
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     */
    public function update(array $data): void
    {
        foreach ($data as $d) {
            $this->delete_data([$d['sigla'], $d['localId']]);
        }
        $this->insert_data($data);
    }

    /**
     * Insert new data
     *
     * @param array $data
     *
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     */
    public function insert_data(array $data): void
    {
        $sql = 'INSERT IGNORE INTO data (`sigla`, `local_id`, `subfield`, `value`) VALUES (' .
            implode('),(', array_map(function ($line) {
                return '"' . implode('","', $line) . '"';
            }, $data)) .
            ');';
        $this->db->query($sql, Adapter::QUERY_MODE_EXECUTE);
    }

    /**
     * Insert new data for a week
     *
     * @param array $data
     *
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     */
    public function insert_week_data(array $data, $week): void
    {
        $platfrom = $this->db->getPlatform();
        $sql = 'INSERT IGNORE INTO week_data (`sigla`, `local_id`, `subfield`, `value`, `week`) VALUES (' .
            implode('),(', array_map(function ($line) use ($week, $platfrom) {
                $line[] = $week;
                return $platfrom->quoteValueList($line);
            }, $data)) .
            ');';
        $this->db->query($sql, Adapter::QUERY_MODE_EXECUTE);
    }


    /**
     * Delete all data
     */
    public function delete_all(): void
    {
        // Use TRUNCATE instead of DELETE as it is faster
        $sql = 'TRUNCATE TABLE data';
        $this->db->query($sql, Adapter::QUERY_MODE_EXECUTE);
    }

    /**
     * Delete week data
     */
    public function delete_week(int $week): void
    {
        $sql = 'DELETE FROM week_data WHERE `week` = ?';
        $this->db->query($sql, [$week]);
    }

    /**
     * Delete current data
     *
     * @param array $params
     *
     * @throws \Laminas\Db\Adapter\Exception\InvalidArgumentException
     */
    protected function delete_data(array $params): void
    {
        $sql = 'DELETE FROM data WHERE `sigla` = ? AND local_id = ?;';
        $this->db->query($sql, $params);
    }
}