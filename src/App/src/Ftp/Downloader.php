<?php

/**
 * Class Downloader
 *
 * PHP version 7
 *
 * Copyright (C) Moravian Library 2020.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  App\Ftp
 * @author   Josef Moravec <moravec@mzk.cz>
 * @license  https://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://knihovny.cz Main Page
 */

namespace App\Ftp;

class Downloader
{
    protected string $host = 'ftp.nkp.cz';
    protected string $username;
    protected string $password;

    protected string $urlFormat = 'ftp://%s:%s@%s/../download/%s';

    public function __construct(array $config)
    {
        $this->username = $config['ftp']['username'];
        $this->password = $config['ftp']['password'];
    }

    /**
     * @param string $filename
     *
     * @return mixed Resource to file
     */
    public function download(string $filename)
    {
        $url = sprintf($this->urlFormat, $this->username, $this->password, $this->host, $filename);

        echo "Downloading file $filename from $this->host\n";
        file_put_contents($filename, fopen($url, 'r'));

        if (substr($filename, -3) === ".gz") {
            $filename = substr($filename, 0, -3);
            echo "Decompressing gzip to $filename\n";
               $gzfile = fopen("$filename.gz", 'r');
               file_put_contents($filename, gzdecode(fread($gzfile, filesize("$filename.gz"))));
               fclose($gzfile);
        }
        return fopen($filename, 'r');
    }
}