<?php

declare(strict_types=1);

namespace App\Handler;

use Mezzio\Template\TemplateRendererInterface;
use Psr\Container\ContainerInterface;

class ApiBaseHandlerFactory
{
    public function __invoke(ContainerInterface $container) : ApiBaseHandler
    {
        return new ApiBaseHandler();
    }
}
