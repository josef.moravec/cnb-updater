<?php

declare(strict_types=1);

namespace App\Handler;

use Psr\Container\ContainerInterface;

class CnbHandlerFactory
{
    public function __invoke(ContainerInterface $container) : CnbHandler
    {
        $database = $container->get(\Laminas\Db\Adapter\Adapter::class);
        return new CnbHandler($database);
    }
}
