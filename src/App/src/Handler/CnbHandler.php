<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;
use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class CnbHandler implements RequestHandlerInterface
{
    /**
     * @var Adapter
     */
    protected $database;

    public function __construct(Adapter $database)
    {
        $this->database = $database;
    }

    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $sigla = $request->getAttribute('sigla');
        $week = $request->getAttribute('week');

        $data = $this->getFromDb($sigla, $week);

        return new JsonResponse(
            [
                'sigla' => $sigla,
                'count' => $this->getCount($sigla, $week),
                'cnb' => $data,
            ]
        );
    }

    protected function getFromDb(string $sigla, ?string $week = null): array
    {
        $sql = new Sql($this->database);
        $select = $this->getSelect($sql, $week);
        $select->columns(
            [
                'local_id' => 'd.local_id',
                'cnb_number' => 'd.value'
            ],
            false
        )->where(['d.sigla' => $sigla]);

        $select->order('d.local_id');
        $selectString = $sql->buildSqlString($select);
        $results = $this->database->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        return $results->toArray();
    }

    protected function getCount(string $sigla, ?string $week = null): int
    {
        $sql = new Sql($this->database);
        $select = $this->getSelect($sql, $week);
        $select->columns(['count' => new Expression('COUNT(*)')], false)
            ->where(['d.sigla' => $sigla]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (int)$results->current()['count'];
    }

    protected function getSelect(Sql $sql, $week = null): Select
    {
        $select = $sql->select();
        if ($week === null) {
            $select->from(['d' => 'data']);
        } else {
            $select->from(['d' => 'week_data'])
                ->where(['d.week' => $week]);
        }
        return $select;
    }
}
