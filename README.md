# ČNB API

## Installation

    git clone https://gitlab.com/josef.moravec/cnb-updater.git cnb

### 1. Install all prerequisities
- a web server
- PHP (7.4), with mbstring and dom extensions
- MariaDB or MySql server

### 2. Configure application

Copy configuration files:

    cp config/autoload/database.local.php.dist config/autoload/database.local.php
    cp config/autoload/ftp.local.php.dist config/autoload/ftp.local.php
    
And edit them

### 3. Create data

Run update script for the first time:
    
    bin/console.php database:update -f
    bin/update_all_week.sh
    
### 4. Setup cron

    0  2  *  *  1 root      /var/www/cnb/bin/cron/cnb-update-weekly.sh

See file `cron.template`

### 5. Setup web server

See `apache.conf.template` for example

## Setup clients

There are client implementations examples in `client` subdirectory
