#!/usr/bin/env php
<?php

chdir(__DIR__ . '/../');

require 'vendor/autoload.php';

use App\Console\RenderJsonCommand;
use App\Console\UpdateDatabaseCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$container = require 'config/container.php';

$application->add(
    new UpdateDatabaseCommand(
        'database:update',
        $container->get(App\Db\Updater::class),
        $container->get(App\Ftp\Downloader::class)
    )
);

$application->add(
    new RenderJsonCommand(
        'json:render',
        $container->get(\Laminas\Db\Adapter\Adapter::class),
    )
);

$application->run();
