#!/bin/bash

week=$(expr $(date +%W) - 1)

if [ "$week" = "-1" ]; then
    week="51"
fi

$(dirname $0)/../console.php database:update -w $week
