#!/usr/bin/perl
#
# Example client for updating CNB in local bibliographic base
# Need to be run under koha instance user (use 'koha-shell' command)
#
# TODO:
#  - Connect to live API, when it is public
#  - Add ability for weekly updates

use Modern::Perl;

use JSON qw/decode_json/;
use MARC::Field;

use C4::Biblio;
use Koha::Biblio::Metadatas;

my $filename = shift or die "Usage: $0 FILENAME\n";

my $json_text = do {
    open(my $json_fh, "<:encoding(UTF-8)", $filename) or die("Can't open file '$filename': $!\n");
    local $/;
    <$json_fh>
};

my $data = decode_json($json_text);
my $modified = 0;
my $metadata;

foreach my $d (@$data) {
    $metadata = Koha::Biblio::Metadatas->find({ 'biblionumber' => $d->{id} });
    if ($metadata) {
        my $record = $metadata->record;
        my @fields015 = $record->field('015');
        if (!@fields015) {
            print "Updating record " . $d->{id} . "\n";
            my $field = MARC::Field->new('015','','','a' => $d->{cnb});
            $record->insert_fields_ordered($field);
	    my $frameworkcode = C4::Biblio::GetFrameworkCode($d->{id});
	    C4::Biblio::ModBiblio($record, $d->{id}, $frameworkcode);
	    $modified++;
        }
    }
}

print "\nNumber of modified records: $modified\n";


